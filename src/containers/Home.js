import React from 'react'
import { withSiteData } from 'react-static'
import { Container, Row, Col } from 'reactstrap';
import {Helmet} from "react-helmet";
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser'
//
import Map from './Maps';

var heroStyle = {
  backgroundImage: 'url(https://s3-us-west-2.amazonaws.com/attorneywebbuilder/clients/template-01/hero.jpg)',
};

export default withSiteData(({ websiteData }) => (
  <article>
    <Helmet>
      <script defer src="/fontawesome/js/all.js"></script>
    </Helmet>
    <section id="home" style={heroStyle}>
      <Container>
        <Row>
          <Col lg="7">
            {ReactHtmlParser(websiteData.welcome.content)}
            {console.log(websiteData)}
          </Col>
        </Row>
      </Container>
    </section>
    <section id="firm">
      <Container>
        <Row>
          <Col sm="12">
            <div className="overview">
              <i className="fas fa-shield-check"></i>
              {ReactHtmlParser(websiteData.firmAbout)}
            </div>
          </Col>
        </Row>
      </Container>
    </section>
    <section id="practice">
      <Container>
        <Row>
          <Col sm="4">
            <div className="single-area">
              <h3>family law</h3>
            </div>
          </Col>
          <Col sm="4">
            <div className="single-area">
              <h3>personal injury</h3>
            </div>
          </Col>
          <Col sm="4">
            <div className="single-area">
              <h3>estate planning</h3>
            </div>
          </Col>
          <Col sm="4">
            <div className="single-area">
              <h3>labor law</h3>
            </div>
          </Col>
          <Col sm="4">
            <div className="single-area">
              <h3>business law</h3>
            </div>
          </Col>
          <Col sm="4">
            <div className="single-area">
              <h3>criminal &amp; dui</h3>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
    <section id="action">
      <Container>
        <Row>
          <Col sm="12">
            {ReactHtmlParser(websiteData.firmAction)}
          </Col>
        </Row>
      </Container>
    </section>
    <section id="staff">
      <Container>
        <Row>
          <Col sm="6">
            <div className="single-staff">
              <div className="staff-photo"></div>
              <h2>Jared Smith</h2>
              <h4>Partner</h4>
              <p>Jared received his Bachelor of Arts Degree with honors and distinction from George Washington University in 1984.</p>
              <a href="mailto:jared@email.com">jared@email.com</a> <a href="tel:555-765-4321">555.765.4321</a>
            </div>
          </Col>
          <Col sm="6">
            <div className="single-staff">
              <div className="staff-photo"></div>
              <h2>Jared Smith</h2>
              <h4>Partner</h4>
              <p>Jared received his Bachelor of Arts Degree with honors and distinction from George Washington University in 1984.</p>
              <a href="mailto:jared@email.com">jared@email.com</a> <a href="tel:555-765-4321">555.765.4321</a>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
    <section id="awards">
      <Container>
        <Row>
          <Col sm="12">
            <img src="https://s3-us-west-2.amazonaws.com/attorneywebbuilder/clients/template-01/awards.jpg" />
          </Col>
        </Row>
      </Container>
    </section>

      <Map />

    <section id="contact">
      <Container>
        <Row>
          <Col sm="6">
            <h1>When life turns upside down we <strong>know</strong> how to <strong>help</strong>!</h1>
            <p>Our firm is committed to providing the highest quality legal services to our clients. We work with our clients to reach cost-effective, creative resolutions to all of their legal matters.</p>
            <div className="social">
              <a href="#"><i className="fab fa-facebook-square"></i></a>
              <a href="#"><i className="fab fa-twitter-square"></i></a>
              <a href="#"><i className="fab fa-linkedin"></i></a>
              <a href="#"><i className="fab fa-instagram"></i></a>
              <a href="#"><i className="fab fa-google-plus-square"></i></a>
              <a href="#"><i className="fab fa-pinterest-square"></i></a>
              <a href="#"><i className="fab fa-youtube-square"></i></a>
              <a href="#"><i className="fab fa-vimeo-square"></i></a>
            </div>
            <a href="#contact" className="btn btn-red"><i className="fas fa-phone"></i> 555.765.4321</a>
          </Col>
          <Col sm="6">
            <form>
              <input type="text" placeholder="Full Name" />
              <input type="email" placeholder="Email" />
              <input type="text" placeholder="Phone" />
              <textarea placeholder="How Can We Help You?"></textarea>
              <input type="submit" className="btn btn-red" value="Submit" />
            </form>
            <div className="disclaimer">The sending of this form is not intended to create, and receipt of it does not constitute, an attorney-client relationship. Anything you send to anyone at our Firm will not be confidential or privileged unless we have agreed to represent you. If you submit this form, you confirm that you have read and understand this notice.</div>
          </Col>
        </Row>
      </Container>
    </section>
  </article>
))
